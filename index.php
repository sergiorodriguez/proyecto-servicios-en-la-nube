<?php

session_start();

if(isset($_SESSION['correo'])){
	$_SESSION['msg'] = "Debes iniciar sesión para ver la página";
	header("location: login.php");
}

if(isset($_GET['logout'])){
	session_destroy();
	unset($_SESSION['correo']);
	header("location: login.php");
}

?>

 <!DOCTYPE html>
 <html>
 <head>
 	<title>Home</title>
 </head>
 <body>

 	<h1>Página principal</h1>
 	<?php
 	if(isset($_SESSION['success'])) : ?>

 	<div>
 		<h3>
 			<?php
 			echo $_SESSION['success'];
 			unset($_SESSION['success']):
 			 ?>
 		</h3>
 	</div>

 <?php endif ?>

 <?php if(isset($_SESSION['correo'])) : ?>
 	<h3>
 		Bienvenido <strong><?php echo $_SESSION['correo']; ?></strong>
 	</h3>
 	<button><a href="index.php?logout='1'"></a></button> 

 <?php endif ?>
 </body>
 </html>