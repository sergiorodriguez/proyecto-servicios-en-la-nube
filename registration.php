<?php include('server.php') ?>

<!DOCTYPE html>
<html>
<head>
	<title>Mail Servicios en la Nube</title>
</head>
<body>

	<div class="container">

		<div class="header">
			<h2>Registrar</h2>
		</div>

		<form action="registration.php" method="post">

			<?php include('erros.php') ?>

			<div>
				<label for="Correo">Correo: </label>
				<input type="text" name="correo" required>
			</div>

			<div>
				<label for="password">Password: </label>
				<input type="password" name="password" required>
			</div>

			<button type="submit" name="register_user">Registrar</button>

			<p>Ya estás registrado? <a href="login.php"><b>Iniciar Sesión</b></a></p>
			
		</form>
		
	</div>

</body>
</html>