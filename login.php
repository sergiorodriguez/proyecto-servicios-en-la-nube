<?php include('server.php') ?>

<!DOCTYPE html>
<html>
<head>
	<title>Mail Servicios en la Nube</title>
</head>
<body>

	<div class="container">

		<div class="header">
			<h2>Iniciar Sesión</h2>
		</div>

		<form action="login.php" method="post">

			<?php include('erros.php') ?>

			<div>
				<label for="Correo">Correo: </label>
				<input type="text" name="correo" required>
			</div>

			<div>
				<label for="password">Password: </label>
				<input type="password" name="password" required>
			</div>

			<button type="submit" name="login_user">Iniciar Sesión</button>

			<p>No estás registrado? <a href="registration.php"><b>Registrar</b></a></p>
			
		</form>
		
	</div>

</body>
</html>